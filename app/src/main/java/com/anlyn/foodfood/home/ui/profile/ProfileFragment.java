package com.anlyn.foodfood.home.ui.profile;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.BindingAdapter;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.anlyn.foodfood.R;
import com.anlyn.foodfood.profile.MyContentsArrangeAdapter;
//import com.anlyn.foodfood.contents.GlideApp;


public class ProfileFragment extends Fragment {
    private ProfileViewModel profileViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        profileViewModel =
                new ViewModelProvider(this).get(ProfileViewModel.class);
        View root = inflater.inflate(R.layout.fragment_notifications, container, false);
        final TextView textView = root.findViewById(R.id.text_notifications);
//        MyContentsArrangeAdapter adapter = new MyContentsArrangeAdapter();
        profileViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        return root;
    }
    @BindingAdapter({"imageUrl"})
    public static void loadUrl(ImageView view,String url){
        view.setImageDrawable(null);
//        FirebaseStorage fs = FirebaseStorage.getInstance();
        //email
//        StorageReference sr = fs.getReference().child()
//        GlideApp.with().load().into(view);
    }

}