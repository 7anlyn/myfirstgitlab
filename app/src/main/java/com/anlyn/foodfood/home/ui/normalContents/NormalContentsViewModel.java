package com.anlyn.foodfood.home.ui.normalContents;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.anlyn.foodfood.contents.FoodAndPictureInfo;

import java.util.List;

public class NormalContentsViewModel extends ViewModel {

    private MutableLiveData<List<FoodAndPictureInfo>> liveDataList;

    public NormalContentsViewModel() {
        liveDataList=new MutableLiveData<>();
    }

    public MutableLiveData<List<FoodAndPictureInfo>> getLiveDataList() {
        return liveDataList;
    }

    public void setLiveDataListt(MutableLiveData<List<FoodAndPictureInfo>> liveDataList) {
        this.liveDataList = liveDataList;
    }
}