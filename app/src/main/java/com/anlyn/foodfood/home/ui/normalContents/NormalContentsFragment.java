package com.anlyn.foodfood.home.ui.normalContents;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.anlyn.foodfood.R;
import com.anlyn.foodfood.contents.ContentsArrangeAdapter;
import com.anlyn.foodfood.contents.ContentsDownloader;
import com.anlyn.foodfood.contents.FoodAndPictureInfo;
import com.anlyn.foodfood.databinding.FragmentNormalContentsBinding;

import java.util.List;

public class NormalContentsFragment extends Fragment {

    private NormalContentsViewModel ViewModel;
    private FragmentNormalContentsBinding binding;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentNormalContentsBinding.inflate(getLayoutInflater());
        ViewModel =
                new ViewModelProvider(this).get(NormalContentsViewModel.class);
        View root = binding.getRoot();

        final RecyclerView recyclerView = root.findViewById(R.id.ContentsRecyleView);
        ContentsDownloader contDown =new ContentsDownloader(getContext(),ViewModel.getLiveDataList());
        final ContentsArrangeAdapter contentsVPAdapter =new ContentsArrangeAdapter(getContext());
        LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        manager.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(contentsVPAdapter);
        contDown.imagedataAccess();

        ViewModel.getLiveDataList().observe(getViewLifecycleOwner(), new Observer<List<FoodAndPictureInfo>>() {
            @Override
            public void onChanged(List<FoodAndPictureInfo> list) {
                Log.d("",list.get(0).getTitle());
                contentsVPAdapter.setmItems(list);
                recyclerView.scrollToPosition(list.size()-1);
            }
        });


        return root;
    }
//
    void init(){
            //LiveData -< contentsDownloader

//        InputContentsImage inputContentsImage=new InputContentsImage(getApplicationContext());
//        inputContentsImage.setRecyclerView(recyclerView);
//        contDown.setInputContentsImage(inputContentsImage);
//
//        contDown.addObserver(inputContentsImage);
//        contDown.imagedataAccess();

        //contents_format.xml contentsVp2

    }
}