package com.anlyn.foodfood.profile;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import com.anlyn.foodfood.BR;
import com.anlyn.foodfood.R;
import com.anlyn.foodfood.contents.FoodAndPictureInfo;

import java.io.Serializable;
import java.util.List;

public class MyContentsArrangeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    private List<FoodAndPictureInfo> list;
    MyContentsArrangeAdapter(List<FoodAndPictureInfo> list){
        this.list = list;
    }

    public void setList(List<FoodAndPictureInfo> list) {
        this.list = list;
        notifyDataSetChanged();
    }



    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        ViewDataBinding binding = DataBindingUtil.inflate(inflater,R.layout.my_content_cell,parent,false);

        if (viewType == VIEW_TYPE_ITEM) {
            return new ItemHolder(binding);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return new LoadingHolder(view);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof ItemHolder) {
            ((ItemHolder)holder).bind(list.get(position),list.get(position).getFileUrl());
        }else {
//            ((LoadingHolder)holder).progressBar.
        }
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return list ==null ? 0 : list.size();
    }

    public class ItemHolder extends RecyclerView.ViewHolder{
        private ViewDataBinding binding;
        private List<String> surlList;
        public ItemHolder(@NonNull ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding=binding;
        }

        void bind(Object obj,List<String> urls){
            binding.setVariable(BR.info,obj);
            binding.setVariable(BR.callBack,new Listener(urls));
            binding.executePendingBindings();
        }
    }
    public class LoadingHolder  extends RecyclerView.ViewHolder{
            ProgressBar progressBar;
        public LoadingHolder(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    public class Listener implements View.OnClickListener{
        private final List<String> urls;
        public Listener(List<String> urls) {
            this.urls = urls;
        }

        @Override
        public void onClick(View v) {
            Activity activity = (Activity) v.getContext();
            Intent intent = new Intent(activity.getApplicationContext(),AfterItemClickContentsActivity.class);
            intent.putExtra("urls",(Serializable) urls);
            activity.startActivity(intent);
        }
    }
}
