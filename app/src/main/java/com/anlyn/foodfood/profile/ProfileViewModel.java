package com.anlyn.foodfood.profile;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.anlyn.foodfood.contents.FoodAndPictureInfo;

import java.util.ArrayList;
import java.util.List;

public class ProfileViewModel extends ViewModel {
    private MutableLiveData<List<FoodAndPictureInfo>> liveDataList;
    public ProfileViewModel(){
        liveDataList= new MutableLiveData<>();
        List<FoodAndPictureInfo> rowsArrayList = new ArrayList<>();
        int i=0;
        while (i<10){
            FoodAndPictureInfo info = new FoodAndPictureInfo();
            info.setTitle("title"+i);
            rowsArrayList.add(info);
            i++;
        }
        liveDataList.setValue(rowsArrayList);
    }
    public MutableLiveData<List<FoodAndPictureInfo>> getLiveDataList() {
        return liveDataList;
    }
}
