package com.anlyn.foodfood.profile;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.viewbinding.ViewBinding;

import com.anlyn.foodfood.R;
import com.anlyn.foodfood.contents.ContentsVPAdapter;
import com.anlyn.foodfood.databinding.ContentsFormatBinding;

import java.io.Serializable;
import java.util.List;

public class AfterItemClickContentsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
//        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.contents_format,null,false);
        ContentsFormatBinding binding = ContentsFormatBinding.inflate(getLayoutInflater());
        ViewBinding bindingUtil = DataBindingUtil.inflate(inflater, R.layout.contents_format,null,false);
        setContentView(binding.getRoot());
        init(binding);
    }

    void init(ContentsFormatBinding binding){
        List<String> list = (List<String>) getIntent().getSerializableExtra("urls");
        ContentsVPAdapter adapter=new ContentsVPAdapter(getApplicationContext(),list);
        binding.ConentsVP2.setAdapter(adapter);
    }
}
