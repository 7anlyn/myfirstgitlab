package com.anlyn.foodfood.profile;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.BindingAdapter;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.ImageView;

import com.anlyn.foodfood.contents.ContentsArrangeAdapter;
import com.anlyn.foodfood.contents.ContentsDownloader;
import com.anlyn.foodfood.contents.FoodAndPictureInfo;
import com.anlyn.foodfood.contents.GlideApp;
import com.anlyn.foodfood.contents.LoadMoreContents;
import com.anlyn.foodfood.databinding.ActivityProfileBinding;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


public class ProfileActivity extends AppCompatActivity {
    private ProfileViewModel viewModel;
    private ActivityProfileBinding binding;
    private MyContentsArrangeAdapter adapter;
    private Boolean isLoading =false;
    private int startAt=0;
    private int endAt=1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityProfileBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        viewModel =
                new ViewModelProvider(this).get(ProfileViewModel.class);

        RecyclerView recyclerView = initRecyclerView();
        recyclerView.setLayoutManager(new LinearLayoutManager(ProfileActivity.this));
        adapter =new MyContentsArrangeAdapter(viewModel.getLiveDataList().getValue());
        recyclerView.setAdapter(adapter);
        viewModel.getLiveDataList().observe(this, new Observer<List<FoodAndPictureInfo>>() {
            @Override
            public void onChanged(List<FoodAndPictureInfo> list) {
                Log.d("size",list.size()+"");
                adapter.setList(list);
            }
        });


    }
    RecyclerView initRecyclerView(){
        RecyclerView recyclerView = binding.myContentsSumaryRecylerview;
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
//                List<FoodAndPictureInfo> rowsArrayList = viewModel.getLiveDataList().getValue();
//                if(rowsArrayList.size()==0) {
//                    LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
//                    if (!isLoading) {
//                        if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == rowsArrayList.size() - 1) {
//                            //bottom of list!
//                            loadMore();
//                            isLoading = true;
//                        }
//                    }
//                }
        };

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                List<FoodAndPictureInfo> rowsArrayList = viewModel.getLiveDataList().getValue();
                if (!isLoading) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == rowsArrayList.size() - 1) {
                        //bottom of list!
                        loadMore();
//                        LoadMoreContents load = new LoadMoreContents(viewModel.getLiveDataList(),adapter,getApplicationContext());
//                        load.execute();
//                        isLoading = true;
                    }
                }
            }
        });

        return recyclerView;
    }

    void loadMore(){
        final List<FoodAndPictureInfo> rowsArrayList = viewModel.getLiveDataList().getValue();
        isLoading = false;
      new Thread(new Runnable() {
          @Override
          public void run() {
              rowsArrayList.add(null);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    adapter.notifyItemInserted(rowsArrayList.size()-1);
                    rowsArrayList.remove(rowsArrayList.size() - 1);
                    int scrollPosition = rowsArrayList.size();
                    adapter.notifyItemRemoved(scrollPosition);
                }
            });

              ContentsDownloader downloader = new ContentsDownloader(getApplicationContext(),viewModel.getLiveDataList());
              downloader.setStateLoading(isLoading);
              downloader.imagedataAccess();
              startAt=+3;
          }
      }).start();

//        Observable.fromCallable(new Callable<Object>() {
//            @Override
//            public Object call() throws Exception {
//                final List<FoodAndPictureInfo> rowsArrayList = viewModel.getLiveDataList().getValue();
//                rowsArrayList.add(null);
//                adapter.notifyItemInserted(rowsArrayList.size()-1);
//                isLoading = false;
//                rowsArrayList.remove(rowsArrayList.size() - 1);
//                int scrollPosition = rowsArrayList.size();
//                adapter.notifyItemRemoved(scrollPosition);
//                ContentsDownloader downloader = new ContentsDownloader(getApplicationContext(),viewModel.getLiveDataList());
//                downloader.imagedataAccess();                return false;
//            }
//        })
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Consumer<Object>() {
//                    @Override
//                    public void accept(Object result) throws Throwable {
//                        //Use result for something
//                    }
//                });

    }



    @BindingAdapter("imageUrl")
    public static void loadUri(ImageView view , List<String> list){
        if(list==null)
            return;
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference ref = storage.getReference().child(list.get(0));
        Log.d("path",list.get(0));
        RequestOptions options = new RequestOptions();
        GlideApp.with(view.getContext())
                .load(ref)
                .into(view);

    }

}