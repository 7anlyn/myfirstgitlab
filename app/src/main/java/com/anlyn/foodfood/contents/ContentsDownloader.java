package com.anlyn.foodfood.contents;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;

public class ContentsDownloader extends Observable {
    FirebaseStorage storage = FirebaseStorage.getInstance();
    StorageReference storageReference =storage.getReference();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("image");
    private Context context;
    private int startAt;
    private int endAt;
    private Boolean loading;
    private MutableLiveData<List<FoodAndPictureInfo>> liveData;
    private static String currentTime = "9999-99-99 00:00:00";
    public ContentsDownloader(Context context, MutableLiveData<List<FoodAndPictureInfo>> liveData){
        this.context=context;
        this.liveData = liveData;
    }

    public void setStateLoading(Boolean loading){
       this.loading=loading;
    }
    public void imagedataAccess( ) {
        //날짜 기준 내림차순 처음 5개//currentTime
        final String time = currentTime;
        myRef.orderByChild("currentTime").endAt(time).limitToLast(3).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshots) {
                List<FoodAndPictureInfo> foodInfoList= new ArrayList<>();
                List<FoodAndPictureInfo> listInliveData = liveData.getValue();
                FoodAndPictureInfo foodAndPictureInfo = null;
                Log.d("getKey", snapshots.getKey().toString());
                    //= new ArrayList<>();
                int limit=0;
                    for (DataSnapshot data : snapshots.getChildren()) {
//                        if(limit==0 && snapshots.getChildrenCount()!=1){
//                            foodAndPictureInfo = data.getValue(FoodAndPictureInfo.class);
//                            currentTime = foodAndPictureInfo.getCurrentTime();
//                            limit++;
//                            continue;
//                        }else if(snapshots.getChildrenCount()==1){
//                            currentTime = "0000-00-00 00:00:00";
//                        }
                        Log.d("getValue", data.getKey().toString());
                        foodAndPictureInfo = data.getValue(FoodAndPictureInfo.class);
                        foodInfoList.add(foodAndPictureInfo);
                        Log.d("getValue", foodAndPictureInfo.getTitle());
                    }
                Collections.reverse(foodInfoList);
//                currentTime=foodInfoList.get(0).getCurrentTime();
                    for(FoodAndPictureInfo info:foodInfoList)
                        listInliveData.add(info);
                Log.d("aa",""+foodInfoList.size());
                liveData.setValue(listInliveData);
                loading=true;

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.d("error", error.toString());
            }
        });

    }

}
