package com.anlyn.foodfood.contents;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.anlyn.foodfood.R;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.List;

public class ContentsVPAdapter extends RecyclerView.Adapter<ContentsVPAdapter.Holder> {
    private List<String> mItems;
    Context context;
    FirebaseStorage storage = FirebaseStorage.getInstance();
    //
    StorageReference storageReference =null;
    public  ContentsVPAdapter(Context context, List<String> mItems) {
        this.context=context;
        this.mItems = mItems;
    }

    @NonNull
    @Override
    public ContentsVPAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.about_send_imageview, parent, false);
        return new ContentsVPAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ContentsVPAdapter.Holder holder, int position) {
        //참조에서 참조로 접근 하지 말 것 , firebaseSorage-> 참조
        storageReference =storage.getReference().child(mItems.get(position));
        GlideApp.with(context)
                .load(storageReference)
                .into(holder.imageView);


    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        ImageView imageView;
        public Holder(@NonNull View itemView) {
            super(itemView);
            imageView=itemView.findViewById(R.id.contents_ImageView);


        }
    }
}
