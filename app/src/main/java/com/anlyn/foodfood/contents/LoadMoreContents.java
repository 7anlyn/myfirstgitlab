package com.anlyn.foodfood.contents;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.anlyn.foodfood.profile.MyContentsArrangeAdapter;

import java.util.List;

public class LoadMoreContents extends AsyncTask<Void,Void,Void> {

    public LoadMoreContents(MutableLiveData<List<FoodAndPictureInfo>> liveData, MyContentsArrangeAdapter adapter,Context context) {
        this.liveData = liveData;
        this.adapter = adapter;
        this.context = context;
    }
    Context context;
    private List<FoodAndPictureInfo> rowsArrayList;
    private MutableLiveData<List<FoodAndPictureInfo>> liveData;
    private MyContentsArrangeAdapter adapter;

    @Override
    protected void onPreExecute() {
        rowsArrayList = liveData.getValue();
        rowsArrayList.add(null);
        adapter.notifyItemInserted(rowsArrayList.size()-1);
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        adapter.notifyDataSetChanged();
//        isLoading = false;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        rowsArrayList.remove(rowsArrayList.size() - 1);
        int scrollPosition = rowsArrayList.size();
        adapter.notifyItemRemoved(scrollPosition);
        int currentSize = scrollPosition;
        int nextLimit = currentSize + 10;
        Log.d("sizeee",currentSize+"");
        ContentsDownloader downloader = new ContentsDownloader(context,liveData);
        downloader.imagedataAccess();
        Log.d("d","ddd");

        return null;
    }

}
