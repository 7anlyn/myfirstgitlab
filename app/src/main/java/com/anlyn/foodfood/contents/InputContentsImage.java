package com.anlyn.foodfood.contents;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class InputContentsImage implements Observer {
    FirebaseStorage storage = FirebaseStorage.getInstance();
    StorageReference storageReference =storage.getReference();
    private Context context;
    InputContentsImage(Context context){
        this.context=context;
    }
    private RecyclerView recyclerView;

    ArrayList<FoodAndPictureInfo> foodInfoList=new ArrayList<>();
    public ArrayList<Bitmap> getBitmapList() {
        return bitmapList;
    }

    private ArrayList<Bitmap> bitmapList=null;



    void setRecyclerView(RecyclerView recyclerView){
        this.recyclerView=recyclerView;
    }


    Bitmap byteToBitmap(byte[] bytes){
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }
    void imageByteDownload(final ArrayList<FoodAndPictureInfo> foodInfoList){
        final long FIVE_MEGABYTE = 8*1024 * 1024;
        //while()
        final ArrayList<ContentsDataMgemnt> cdmList=new ArrayList<>();
        for(FoodAndPictureInfo f :foodInfoList) {
            bitmapList=new ArrayList<>();
            final ArrayList<String> pathList = f.getFileUrl();
            int i = 0;

            for (String path : pathList) {
                Log.d("path",path);
                final int limitCount =pathList.size();
                i++;
                final int count = i;
                final StorageReference ref = storageReference.child(path);
                ref.getBytes(FIVE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                    int j=0;
                    int ic=limitCount;
                    @Override
                    public void onSuccess(byte[] bytes) {
                        //이미지 설정
                        Bitmap bitmap = byteToBitmap(bytes);
                        bitmapList.add(bitmap);

                        if (limitCount == count) {
                            ContentsDataMgemnt cdm = new ContentsDataMgemnt();
                            cdm.setImageList(bitmapList);

                            cdmList.add(cdm);
                            Log.d("d", cdmList.size() + "");
                            if(cdmList.size()==foodInfoList.size()){
                                Log.d("bitmap","완료");
                                //showImage(cdmList);
                            }
                            //  inputImage();
                        }
                        Log.d("d", "이미지 불러오기");
                        //inputImage();
                    }

                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                    }

                });
            }
        }
    }

    void showImage(ArrayList<FoodAndPictureInfo> list){
        ContentsArrangeAdapter contentsVPAdapter =new ContentsArrangeAdapter(context);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(contentsVPAdapter);
    }

    @Override
    public void update(Observable o, Object arg) {
        if(o instanceof ContentsDownloader){
            ArrayList<FoodAndPictureInfo> list = (ArrayList<FoodAndPictureInfo>)arg;
            showImage(list);
            //imageByteDownload(list);
           // Log.d("arg:" ,list.get(0).getTitle());
        }
    }
}
