package com.anlyn.foodfood.contents;

import android.graphics.Bitmap;

import java.util.ArrayList;

class ContentsDataMgemnt {
    private String title;
    private ArrayList<Bitmap> imageList;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<Bitmap> getImageList() {
        return imageList;
    }

    public void setImageList(ArrayList<Bitmap> imageList) {
        this.imageList = imageList;
    }
}
