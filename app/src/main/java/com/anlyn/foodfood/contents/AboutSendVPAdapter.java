package com.anlyn.foodfood.contents;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import com.anlyn.foodfood.BR;
import com.anlyn.foodfood.R;
import java.util.List;

public class AboutSendVPAdapter extends RecyclerView.Adapter<AboutSendVPAdapter.Holder> {
    private List<Uri> mItems;
    private Context context;
    public AboutSendVPAdapter(Context context, List<Uri> mItems) {
        this.context=context;
        this.mItems = mItems;
    }

    @NonNull
    @Override
    public AboutSendVPAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ViewDataBinding binding = DataBindingUtil.inflate(inflater,R.layout.about_send_imageview,parent,false);
        return new Holder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull AboutSendVPAdapter.Holder holder, int position) {
        holder.bind(mItems.get(position));
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        ViewDataBinding binding;
        public Holder(@NonNull ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
        void bind(Object object){
            binding.setVariable(BR.obj,object);
            binding.executePendingBindings();
        }
    }
}
