package com.anlyn.foodfood.contents;

import java.util.ArrayList;

//foodContentsCompostionData
public class FoodAndPictureInfo {
    //refactoring key를 등록해야 함
    private ArrayList<String> tag;
    private String currentTime;
    private ArrayList<String> fileUrl;
    private String title;
    private String email;

    public ArrayList<String> getTag() {
        return tag;
    }

    public void setTag(ArrayList<String> tag) {
        this.tag = tag;
    }

    public String getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(String currentTime) {
        this.currentTime = currentTime;
    }

    public ArrayList<String> getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(ArrayList<String> fileUrl) {
        this.fileUrl=fileUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
