package com.anlyn.foodfood.contents;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import com.anlyn.foodfood.R;

import java.util.ArrayList;
import java.util.List;

public class ContentsArrangeAdapter extends RecyclerView.Adapter<ContentsArrangeAdapter.Holder> {
    private List<FoodAndPictureInfo> mItems;
    Context context;
    public ContentsArrangeAdapter(Context context) {
        mItems=new ArrayList<>();
        this.context=context;
    }

    public void setmItems(List<FoodAndPictureInfo> mItems) {
        this.mItems = mItems;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ContentsArrangeAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.contents_format, parent, false);
        return new ContentsArrangeAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ContentsArrangeAdapter.Holder holder, int position) {
        Log.d("url",mItems.get(position).getFileUrl()+"");
        ViewPager2 viewPager2=holder.viewPager2;
        ContentsVPAdapter adapter=new ContentsVPAdapter(context,mItems.get(position).getFileUrl());
        viewPager2.setAdapter(adapter);
        //데이터베이스 접근
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        TextView title;
        ViewPager2 viewPager2;
        public Holder(@NonNull View itemView) {
            super(itemView);

            viewPager2=itemView.findViewById(R.id.ConentsVP2);
            title = itemView.findViewById(R.id.contents_title_tv);

        }
    }
}