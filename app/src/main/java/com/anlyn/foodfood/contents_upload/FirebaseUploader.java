package com.anlyn.foodfood.contents_upload;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.anlyn.foodfood.contents.FoodAndPictureInfo;
import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.sql.Ref;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

class FirebaseUploader {
    private final Context context;
    private FirebaseStorage storage = FirebaseStorage.getInstance();
    private StorageReference storageReference =storage.getReference();
    private FirebaseDatabase database ;
    private DatabaseReference myRef =FirebaseDatabase.getInstance().getReference("image");

    FoodAndPictureInfo fccd=null;
    FirebaseUploader(Context context){
        this.context = context;
    }
    void setFccd(FoodAndPictureInfo fccd) {
        this.fccd = fccd;
    }

    void upload(final List<Uri> imageUri, final String type, final String emailID){
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... voids) {
                uploadImage(imageUri,type,emailID);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
            }
        }.execute();
    }

    void uploadDataInfo(ArrayList<String> list) {
        //list는 나중에 수정 필요
        this.fccd.setFileUrl(list);
        myRef.push().setValue(fccd);
    }


    //https://www.geeksforgeeks.org/android-how-to-upload-an-image-on-firebase-storage/
    void uploadImage(List<Uri> imageUri, final String type, String emailID) {

        Log.d("email",emailID+"?");

        if (imageUri != null) {
//            final ProgressDialog progressDialog = new ProgressDialog((Activity)context);
//            progressDialog.setTitle("이미지 업로드...");
            //UUID 기기식별??
            ArrayList<String> urlList = new ArrayList<>();
            //ID 바꿈
            for (int i = 0; i < imageUri.size(); i++) {
                //fccd
                urlList.add(emailID+"/" + UUID.randomUUID().toString());

            }
            uploadDataInfo(urlList);
            //파일입력
            for (int i = 0; i < imageUri.size(); i++) {

                //한꺼번에 묶는다면 ? ->현재시간으로 정렬
                StorageReference ref = storageReference.child(urlList.get(i));
                ref.putFile(imageUri.get(i))
                        .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                                //progressDialog.dismiss();
                                Log.d("success", "업로드 완료 ");
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                // progressDialog.dismiss();
                                Log.d("error", e.getMessage());
                            }
                        });

            }
        }
    }
}
