package com.anlyn.foodfood.contents_upload;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.BindingAdapter;
import androidx.viewpager2.widget.ViewPager2;

import com.anlyn.foodfood.contents.AboutSendVPAdapter;
import com.anlyn.foodfood.contents.ContentsVPAdapter;
import com.anlyn.foodfood.contents.FoodAndPictureInfo;
import com.anlyn.foodfood.databinding.ActivityContentsUploadBinding;
import com.bumptech.glide.Glide;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class ContentsUploadActivity extends AppCompatActivity {
    private final int PICK_IMAGE_REQUEST=1010;
    private Uri[] imageUri;
    private ActivityContentsUploadBinding binding;
    private FoodAndPictureInfo info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityContentsUploadBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        bind(binding);
        imageSelection();
    }
    //image select
    void imageSelection(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE,true);
        intent.setAction(Intent.ACTION_PICK);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK) {
            //add&commit
            //stash
            //이미지 사이즈 조정
            if (data.getClipData() != null) {
                int count = data.getClipData().getItemCount(); //evaluate the count before the for loop --- otherwise, the count is evaluated every loop.
                imageUri=new Uri[count];
                for (int i = 0; i < count; i++) {
                    imageUri[i] = data.getClipData().getItemAt(i).getUri();
                }
                //do something with the image (save it to some directory or whatever you need to do with it here)
            } else if (data.getData() != null) {
                imageUri=new Uri[1];
                imageUri[0]= data.getData();
                //do something with the image (save it to some directory or whatever you need to do with it here)
            }
            ViewPager2 viewPager2 = binding.SendConentsVP;
            List<Uri> list = Arrays.asList(imageUri);
            Log.d("", list.get(0).toString());
            AboutSendVPAdapter adapter =new AboutSendVPAdapter(this,list);
            viewPager2.setAdapter(adapter);
        }
    }

    void bind(ActivityContentsUploadBinding binding){
        final EditText titleEditText =binding.titleEditText;
        Intent intent =getIntent();
        final String email = intent.getStringExtra("email");
//        Log.d("email",email);
        Button button = binding.sendBtn;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                info=new FoodAndPictureInfo();
                long curTime = System.currentTimeMillis();
                SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                String time = timeFormat.format(new Date(curTime));
                List<Uri> list = Arrays.asList(imageUri);

                info.setTitle(titleEditText.getText().toString());
                info.setCurrentTime(time);
                info.setEmail(email);
                FirebaseUploader uploader =new FirebaseUploader(getApplicationContext());
                uploader.setFccd(info);
                uploader.upload(list,"",email);
            }
        });
    }
    @BindingAdapter("loadImage")
    public static void loadImage(ImageView imageView, Uri uri){
        Context context = imageView.getContext();
        Glide.with(context).load(uri).into(imageView);
    }

}